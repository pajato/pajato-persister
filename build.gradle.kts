plugins {
    alias(libs.plugins.kmp.lib)
}

group = "com.pajato"
version = "0.10.2"
description = "The Pajato persistence feature, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting { dependencies { implementation(libs.kotlinx.serialization.json) } }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)
        }
    }
}
