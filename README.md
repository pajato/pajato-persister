# pajato-persister

## Description

Briefly, [System Prevalence](https://en.wikipedia.org/wiki/System_prevalence) is a journaling facility that provides
very simple and efficient persistence for software projects. Follow the link for more information.

All Pajato software projects utilize this model to provide persistence. When loading data at startup, System Prevalence
will read JSON records into a cache such that all instance creations, modifications and deletions are fully installed
and replaced on the file system with just creation records.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the feature.

## Test Cases

### Overview

The table below identifies the system prevalence unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions).

| Filename Prefix  | Test Case Name                                                     |
|------------------|--------------------------------------------------------------------|
| SystemPrevalence | When reading from an empty file, verify the cache size is 0        |
|                  | When reading from a non-empty file, verify the cache size is not 0 |
|                  | When reading from a non-empty file, verify the cache size is not 0 |
|                  | When persisting data, verify behavior                              |
|                  | When loading data using sorting, verify behavior                   |

### Notes
