package com.pajato.persister

import kotlinx.serialization.Serializable

@Serializable data class TestData(val id: Int, val data: String)
