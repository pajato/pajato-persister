package com.pajato.persister

import com.pajato.test.ReportingTestProfiler
import java.io.File
import java.net.URL
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class SystemPrevalenceUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val delim = "\n"

    @Test fun `When reading from an empty file, verify the cache size is 0`() {
        val url = getResource("empty-data.txt")
        val expectedSize = 0
        val cache: Cache<Int, TestData> = mutableMapOf()
        readAndPruneData(url.toURI(), cache, TestData.serializer(), ::getKeyFromItem)
        assertEquals(expectedSize, cache.size)
    }

    @Test fun `When reading from a non-empty file, verify the cache size is not 0`() {
        val (srcFile, dstFile) = Pair(getFile("multiple-entries.txt"), getFile("buffer.txt"))
        val expectedSize = 2
        val cache: Cache<Int, TestData> = mutableMapOf()
        dstFile.writeText(srcFile.readText())
        readAndPruneData(dstFile.toURI(), cache, TestData.serializer(), ::getKeyFromItem)
        assertEquals(expectedSize, cache.size)
    }

    @Test fun `When reading multiple entries including updates and removes, verify pruning`() {
        val url: URL = getResource("multiple-entries.txt")
        val expectedSize = 2
        val cache: Cache<Int, TestData> = mutableMapOf()
        readAndPruneData(url.toURI(), cache, TestData.serializer(), ::getKeyFromItem)
        assertEquals(expectedSize, cache.size)
    }

    @Test fun `When persisting data, verify behavior`() {
        val json = """{"data":"some json data"}"""
        val expectedContent = "$json\n"
        val url = loader.getResource("file.txt") ?: fail("Could not get persistence file!")
        val file = File(url.toURI()).also { it.writeText("") }
        persist(url.toURI(), json)
        assertEquals(expectedContent, file.readText())
    }

    private fun getFile(path: String): File = File(getResource(path).toURI())

    private fun getResource(path: String) = loader.getResource(path) ?: fail(RESOURCE_ERROR)

    private fun getKeyFromItem(item: TestData) = item.id

    companion object { const val RESOURCE_ERROR = "Could not get resource file!" }
}
