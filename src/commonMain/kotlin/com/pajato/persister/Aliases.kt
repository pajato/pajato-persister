package com.pajato.persister

import kotlinx.serialization.KSerializer

internal typealias Cache<K, T> = MutableMap<K, T>
internal typealias KS<T> = KSerializer<T>
