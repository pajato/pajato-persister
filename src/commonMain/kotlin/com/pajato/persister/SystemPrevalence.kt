package com.pajato.persister

import java.io.File
import java.net.URI

/**
 * Reads data from a file specified by the given URI, updates the cache,
 * and prunes the file by recreating its content after consolidating creates,
 * modifies and deletes
 *
 * @param K The type of the cache key.
 * @param T The type of the cache value.
 * @param uri The URI of the file to read and prune.
 * @param cache The cache to update.
 * @param serializer The serializer used to decode the data.
 * @param getKeyFromItem A function that extracts the key from an item in the cache.
 */
public fun <K, T> readAndPruneData(uri: URI, cache: Cache<K, T>, serializer: KS<T>, getKeyFromItem: (T) -> K) {
    val file = File(uri)
    fun updateCache(line: String) { updateCache(line, cache, serializer, getKeyFromItem) }
    cache.clear().also { file.readLines().forEach { updateCache(it) } }.also { file.writeText("") }
    cache.values.forEach { persist(uri, jsonFormat.encodeToString(serializer, it)) }
}

/**
 * Persists the given JSON string to a file specified by the provided URI.
 *
 * @param uri The URI of the file to persist the JSON to.
 * @param json The JSON string to persist.
 */
public fun persist(uri: URI, json: String): Unit = File(uri).appendText("$json\n")

private fun <K, T> updateCache(line: String, cache: Cache<K, T>, serializer: KS<T>, getKeyFromItem: (T) -> K) {
    val remove = line.startsWith("-")
    val item = jsonFormat.decodeFromString(serializer, if (remove) line.substring(1) else line)
    val key = getKeyFromItem(item)
    if (remove) cache.remove(key) else cache[key] = item
}
